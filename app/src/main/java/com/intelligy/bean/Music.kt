package com.intelligy.bean

data class Music(
    val title: String,
    val path: String,
    val singer: String,
)
