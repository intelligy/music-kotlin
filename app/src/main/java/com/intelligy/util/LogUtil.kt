package com.intelligy.util

import android.content.Context
import android.util.Log
import android.widget.Toast

const val tag = "my log: "

fun log(msg: String) {
    Log.d(tag, msg)
}

/**
 * context 扩展弹窗方法
 */
fun Context.toast(msg: String) {
    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
}